# Implementing backpropagation from scratch

### Todos:
- [ ] show history plot
- [ ] try to interpret weights ??
- [ ] try with different batch sizes
- [ ] try hyperparameter search

## Convergence
The model converges every time. Running the command `python nn.py train` starts training a brand-new neural network with random weights. Moreover, the inputs are randomly shuffled every epoch.
We run an experiment to test convergence: training the network 10 times, with 50000 epochs each (note this is less then what is used for optimal training but 
By running the command `python nn.py test_convergence`
## Hyperparameters Tuning

## Running the code
### Requirements
The latest stable python version is reccomended: `python 3.9.7`.
Moreover, the following packages are required and can be installed via `pip install tqdm ipdb numpy matplotlib`.

### Executable
The program can be run via `python nn.py <command>`. To have a list of the available commands, run:
```
python nn.py -h
```
